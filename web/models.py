from .app import db
from flask.ext.login import UserMixin
from .app import login_manager

class User(db.Model, UserMixin):
	pseudo = db.Column(db.String(100), primary_key = True)
	nom = db.Column(db.String(20))
	prenom = db.Column(db.String(20))
	mail = db.Column(db.String(100))
	type_compte = db.Column(db.String(100)) #utilisateur et administrateur
	mdp = db.Column(db.String(100))
	score = db.Column(db.Integer)
	nbDefaites = db.Column(db.Integer)
	nbVictoires = db.Column(db.Integer)
	cpp = db.Column(db.Integer)
	python = db.Column(db.Integer)
	java = db.Column(db.Integer)
	img = db.Column(db.String(200))

	def get_id(self):
		return self.pseudo

	def __repr__(self):
		return "<Utilisateur (%s>" % (self.pseudo)



@login_manager.user_loader
def load_user(pseudo):
	return User.query.get(pseudo)

class Programme(db.Model):
	id_programme = db.Column(db.Integer, primary_key = True, autoincrement=True)
	contenu_programme = db.Column(db.String(100)) # url envoyant vers ou est le programme
	id_user = db.Column(db.String, db.ForeignKey("user.pseudo"))
	user = db.relationship("User", backref=db.backref("programme", lazy="dynamic"))

	def __repr__(self):
		return "<Programme (%d) %s>" % (self.id_programme, self.contenu_programme)

	def get_id_programme(self):
		return self.id_programme


class Tournoi(db.Model):
	id_tournoi = db.Column(db.Integer, primary_key = True, autoincrement=True)
	nom_tournoi = db.Column(db.String(40))
	date_debut = db.Column(db.String(11)) #DDMMAAAA
	date_fin = db.Column(db.String(11))

	id_jeu = db.Column(db.Integer, db.ForeignKey("jeu.id_jeu"))
	jeu = db.relationship("Jeu", backref=db.backref("tournoi", lazy="dynamic"))

	id_moderateur = db.Column(db.String, db.ForeignKey("user.pseudo"))
	user = db.relationship("User", backref=db.backref("tournoi", lazy="dynamic"))

	def __repr__(self):
		return "<Tournoi (%d) %s>" % (self.id_tournoi)

	def get_id_tournoi(self):
		return self.id_tournoi

class Jeu(db.Model):
	id_jeu = db.Column(db.Integer, primary_key = True, autoincrement=True)
	nom_jeu = db.Column(db.String(100))
	description = db.Column(db.String(200))  # url vers le fichier contenant la description
	objectif = db.Column(db.String(200))
	contrainte = db.Column(db.String(200))

	id_user = db.Column(db.String, db.ForeignKey("user.pseudo"))
	user = db.relationship("User", backref=db.backref("jeu", lazy="dynamic"))

	difficulte = db.Column(db.Integer)

	def __repr__(self):
		return "<Jeu (%d) %s>" % (self.id_jeu)

class Participer(db.Model):
	id_participer = db.Column(db.Integer, primary_key = True, autoincrement=True)

	id_tournoi = db.Column(db.Integer, db.ForeignKey("tournoi.id_tournoi"))
	tournoi = db.relationship("Tournoi", backref=db.backref("participer", lazy="dynamic"))

	id_programme = db.Column(db.Integer, db.ForeignKey("programme.id_programme"))
	programme = db.relationship("Programme", backref=db.backref("participer", lazy="dynamic"))

	id_user = db.Column(db.Integer, db.ForeignKey("user.pseudo"))
	user = db.relationship("User", backref=db.backref("participer", lazy="dynamic"))

	score = db.Column(db.Integer)
	num_classement = db.Column(db.Integer)
	actif = db.Column(db.String(3))
	def __repr__(self):
		return "<Tournoi (%d) %s>" % (self.id_tournoi)

	def get_id_tournoi(self):
		return self.id_tournoi

class Groupe(db.Model):
	id_groupe = db.Column(db.Integer, primary_key = True, autoincrement=True)
	nom_groupe = db.Column(db.String(100))

	id_gerant = db.Column(db.Integer, db.ForeignKey("user.pseudo"))
	user = db.relationship("User", backref=db.backref("groupe", lazy="dynamic"))

class AvoirGroupe(db.Model):
	id_avoirGroupe = db.Column(db.Integer, primary_key = True, autoincrement=True)
	id_user = db.Column(db.Integer, db.ForeignKey("user.pseudo"))
	user = db.relationship("User", backref=db.backref("avoirGroupe", lazy="dynamic"))

	id_groupe = db.Column(db.Integer, db.ForeignKey("groupe.id_groupe"))
	groupe = db.relationship("Groupe", backref=db.backref("avoirGroupe", lazy="dynamic"))

def newUser(userpseudo,userNom,userPrenom,userEmail,type_c,password,image = "defaut.jpg"):
	''' Adds a new user '''
	from hashlib import sha256
	m = sha256()
	m.update(password.encode())
	u = User(pseudo=userpseudo,nom=userNom, prenom=userPrenom, mail=userEmail,type_compte=type_c, mdp=m.hexdigest(), score = 0, nbDefaites = 0, nbVictoires = 0, cpp = 0, java = 0, python = 0,img = image)
	db.session.add(u)
	db.session.commit()

def newProgramme(contenu_prog, pseudoUser):
	''' Adds a new Programme '''
	prog = Programme(contenu_programme = contenu_prog, id_user = pseudoUser)
	db.session.add(prog)
	db.session.commit()

def newTournoi(nom_t, date_d, date_f, id_game, id_mod):
	''' Adds a new tournoi '''
	tournoi = Tournoi(nom_tournoi = nom_t, date_debut = date_d, date_fin = date_f, id_jeu = id_game, id_moderateur = id_mod)
	db.session.add(tournoi)
	db.session.commit()



def newJeu(nomJeu, descr, obj, contr, pseudoUser, dif):
	''' Adds a new jeu '''
	jeu = Jeu(nom_jeu = nomJeu, description = descr, objectif = obj, contrainte = contr, id_user = pseudoUser, difficulte = dif)
	db.session.add(jeu)
	db.session.commit()

def newParticiper(id_tourn, id_prog, id_util, s, num_class, a):
	''' Adds a new participant for the tournoi '''
	p = Participer(id_tournoi = id_tourn, id_programme = id_prog, id_user = id_util, score = s, num_classement = num_class, actif = a)
	db.session.add(p)
	db.session.commit()

def newGroupe(nom_g, gerant):
	''' Adds a new group '''
	groupe = Groupe(nom_groupe = nom_g, id_gerant = gerant)
	db.session.add(groupe)
	db.session.commit()

def newAvoirGroupe(id_u, id_g):
	''' Adds a new member for the group '''
	membre = AvoirGroupe(id_user = id_u, id_groupe = id_g)
	db.session.add(membre)
	db.session.commit()

def getClassement():
	''' Retourne une liste [(pseudo, score, defaites, victoires, classement)] '''
	query = User.query.all()
	queryTrie = sorted(query, key=getScore)
	liste = []
	i = 1
	for elem in queryTrie:
		liste.append((elem.pseudo, elem.score, elem.nbDefaites, elem.nbVictoires, i))
		i += 1
	return liste

def getScore(util):
	''' Recupere le score '''
	return -util.score

def setJava(id_user):
	''' Incremente de 1 l'attribut java -> signifie son experience en java '''
	res = User.query.filter(User.pseudo == id_user).first()
	res.java += 1
	db.session.add(res)
	db.session.commit()

def setPython(id_user):
	''' Incremente de 1 l'attribut python -> signifie son experience en python '''
	res = User.query.filter(User.pseudo == id_user).first()
	res.python += 1
	db.session.add(res)
	db.session.commit()

def setCpp(id_user):
	''' Incremente de 1 l'attribut cpp -> signifie son experience en cpp '''
	res = User.query.filter(User.pseudo == id_user).first()
	res.cpp += 1
	db.session.add(res)
	db.session.commit()

def setImg(id_user, image):
	''' modifie change l'image '''
	res = User.query.filter(User.pseudo == id_user).first()
	res.img = image
	db.session.add(res)
	db.session.commit()

def setScore(id_user, sc):
	''' modifie le scrore '''
	res = User.query.filter(User.pseudo == id_user).first()
	sc = int(sc)
	res.score += sc
	db.session.add(res)
	db.session.commit()

def setNbDefaites(id_user):
	''' modifie le nb de défaites '''
	res = User.query.filter(User.pseudo == id_user).first()
	res.nbDefaites += 1
	db.session.add(res)
	db.session.commit()

def setNbVictoires(id_user):
	''' modifie le nb de victoires '''
	res = User.query.filter(User.pseudo == id_user).first()
	res.nbVictoires += 1
	db.session.add(res)
	db.session.commit()

def getListeAmis(id_util):
	''' renvoie la liste des amis '''
	user = User.query.filter(User.pseudo == id_util).first()
	id_groupes = AvoirGroupe.query.filter(AvoirGroupe.id_user == id_util)
	nb = AvoirGroupe.query.filter(AvoirGroupe.id_user == id_util).count()
	listeAmis = []
	res = []
	if nb >0:
		for elem in id_groupes:
			amis = AvoirGroupe.query.filter(AvoirGroupe.id_groupe == elem.id_groupe)
			for a in amis:
				if a not in listeAmis and a.id_user != id_util:
					listeAmis.append(a)
	for element in listeAmis:
		user_ami = User.query.filter(User.pseudo == element.id_user).first()
		res.append((user_ami.pseudo, user_ami.img))
	return res

def getTournois():
	''' Retourne une liste [(nom_tournois, date_debut, date_fin, nb_participants, nom_moderateur)] '''
	tournois = Tournoi.query.all()   #tous les tournois
	liste = []
	for t in tournois: #t -> tournois
		nb = Participer.query.filter(Participer.id_tournoi == t.id_tournoi).count()
		moderateur = User.query.filter(User.pseudo == t.id_moderateur).first()
		liste.append((t.nom_tournoi, t.date_debut, t.date_fin, nb, moderateur.nom))
	return liste

def getGroupe():
	''' Retourne une liste [(nom_groupe, nb_membres, nom_moderateur)] '''
	groupes = Groupe.query.all() #tous les groupes
	liste = []
	for g in groupes:
		nb = AvoirGroupe.query.filter(AvoirGroupe.id_groupe == g.id_groupe).count()
		moderateur = User.query.filter(User.pseudo == g.id_gerant).first()
		liste.append((g.nom_groupe, nb, moderateur.nom))
	return liste

def getListeJoueurs():
	''' Retourne tous les joueurs qui ne sont pas administrateurs '''
	liste = []
	joueurs = User.query.all()
	for j in joueurs:
		if j.type_compte == "utilisateur":
			liste.append(j.pseudo)
	return liste

def getListeProgramme():
	''' Retourne la liste de tous les programmes '''
	liste = []
	programmes = Programme.query.all()
	for p in programmes:
		liste.append(p)
	return liste

def getProgrammes(pseudo):
	''' Retourne les programmes d'un user '''
	liste = []
	programmes = Programme.query.filter(Programme.id_user == pseudo)
	for p in programmes:
		liste.append(p)
	return liste

def getListeGroupes():
	''' Retourne le tous les goupes '''
	liste = []
	groupes = Groupe.query.all()
	for g in groupes:
		liste.append((g.id_groupe, g.nom_groupe))
	return liste
