


// le nombre de boule de depart
var initialisation = 300;
// La vitesse par defaut pour les boules 
var particleSpeed = 0.2;
// La longeur maximal du pacrour pour chauque boule pour qu'ils restent dans le Canvas
var longeurChemin = 6;
//Les differentes couleurs pour les boules
var couleurs = ["#00A878","#009DDC","#FE5E41","#FCBA04","#0496FF","#DB2B39","#011AA9"];
// taille d'une boule par defaut est 10
var taille= 10;

//initialisation du Canvas
var boules = [];
// Le Canvas prend la taille du fenetre
var windowWidth = window.innerWidth*4;
var windowHeight = window.innerHeight*4;

var canvas = document.createElement('canvas');
var context = canvas.getContext("2d"); 

canvas.id = "canvas";
canvas.width = window.innerWidth*4;
canvas.height = window.innerHeight*4;

document.body.appendChild(canvas);



function Boule (x, y, taille) {
  //position x ,y pour chaque boule
  this.x = x;
  this.y = y;
  
  
  //La taille d'une boule 
  this.taille=taille;

  // Pour deplacer une boule Nous avons besoins de ces prochaines x et y ainsi que la vitesse  
  this.vel = {
    x : _.random(-20, 20)/100,
    y : _.random(-20, 20)/100,
    min : _.random(2, 10),
    max : _.random(10, 100)/10
  }
  
  this.entrainer = [];
  
  this.color = couleurs[_.random(0,couleurs.length)];
}

// cette methode affiche les boules sur le Canvas
Boule.prototype.affichage = function() {
  context.beginPath();
  //Specifier la couleur de la boule
  context.fillStyle = this.color;

  
  // Dessiner la boule
  context.arc(this.x,this.y, this.taille, 0, Math.PI*2);

  var i = this.entrainer.length-1;
   
  context.fill();
};

// Cette fonction modifier la position des boules 
Boule.prototype.modifier = function(){
   

  var forceDirection = {
    x :  _.random(-1, 1),
    y :  _.random(-1, 1),
  };

  if( Math.abs(this.vel.x + forceDirection.x) < this.vel.max)
    this.vel.x += forceDirection.x;
  if( Math.abs(this.vel.y + forceDirection.y) < this.vel.max)
    this.vel.y += forceDirection.y;
  
  this.x += this.vel.x*particleSpeed;
  this.y += this.vel.y*particleSpeed;
  
  if(Math.abs(this.vel.x) > this.vel.min)
      this.vel.x *= 0.99;
  if(Math.abs(this.vel.y) > this.vel.min)
      this.vel.y *= 0.99;
  
  //entrainer
  this.entrainer.push({
    x : this.x,
    y : this.y
  });
  
  this.testLongeurPage();
  
  if(this.entrainer.length > longeurChemin){
    this.entrainer.splice(0,1);
  }
}

// faire en sorte que la boule revient si elle sort de la page 
Boule.prototype.testLongeurPage = function() {
  if( this.x > windowWidth + longeurChemin ){
    this.changerPosition(- longeurChemin, "x");
  } else if( this.x < -longeurChemin) {
    this.changerPosition(windowWidth + longeurChemin, "x");
  }
  if( this.y > windowHeight + longeurChemin){
    this.changerPosition(- longeurChemin, "y");
  } else if( this.y < -longeurChemin) {
    this.changerPosition(windowHeight + longeurChemin, "y");
  }
}
Boule.prototype.changerPosition = function(pos, coor) {
  if(coor == "x") {
    this.x = pos;
    _.chain(this.entrainer).each(function(entrainer) {
      entrainer.x = pos;
    });
  } else if( coor == "y" ) {    
    this.y = pos;
    _.chain(this.entrainer).each(function(entrainer) {
      entrainer.y = pos;
    });
  }
}





function start(){
	context.clearRect(0,0, canvas.width, canvas.height);
	_.chain(boules).each(function(p, index){
    p.modifier();
    p.affichage();
	});
  //Refrecher la page
	requestAnimationFrame(start);
}

/* ---- START ---- */
for (var i = 0; i < initialisation ; i++) {
  
  // la taille de chaque boule entre 5 et 30
  var taille=Math.floor((Math.random() * 30) + 5); 

  // ajouter des nouvelles boule à la liste en spécifient leur position sur la page
  boules.push(new Boule(
		_.random(0, windowWidth),
		_.random(0, windowHeight),
    taille
	));
}

start();




