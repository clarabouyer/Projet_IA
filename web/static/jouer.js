$(document).ready(function()
{
    $("#pageCourante ul li").click(function(){
    	 $(".content").removeClass("isOpen");
        var index = $("#pageCourante ul li").index(this);
        $('.staffSelected').fadeOut(500);
        $('.pageCourante2').removeClass('pageCourante2');
        $(this).addClass('pageCourante2');

        setTimeout(function() {
            $('.staffSelected').removeClass('staffSelected');
            $("#pageCourante2 ul li:eq("+index+")").fadeIn(500).addClass('staffSelected');
        }, 500);
    });
    $("#pageCourante ul li:first-child").click();

});
