//initialisation du Canvas
// A modificier pour adapter  voir JSON

    //Serveur la liste doit etre (x,y,vitesse)
var boules = [];
var particleSpeed = 0.5;
var couleurs= ['#00A878',"#009DDC","#FE5E41","#FCBA04","#0496FF","#DB2B39","#011AA9","#8B0000","#A0522D","#008080","#6A5ACD"];
var longeurChemin = 6;

// Deux thread 1= affiche 2= recuperer donner du serveur.


 /*function afficheJR(liste_boules){
  console.log("affichage");
   for(boule : liste_boules)
    console.log("boule : " + boule);


 }*/

 // fonction qui prend la liste du serveur et remplie var boule avec des boules de la liste
function convertir(liste_boule){
  boules = [];
  for(var i = 0;i<liste_boule.length;++i){
    boules.push(new Boule(liste_boule[i][0],liste_boule[i][1],liste_boule[i][2],liste_boule[i][3]));

  }
}


 //FOnction qui met à jours la liste de boule.
function maj(){
  $.getJSON("/listeBoules/",function(data){ convertir(data);});

}



// Amelioration pour avertir client
// function av(listeMort){
//   for(var i = 0;i<listeMort.length;++i){
//     if(pseudo==listeMort[i]){
//       alert("Boule morte!!!");
//     }
//   }
//
// }
// // Avertir sir sa boule est manger
// function avertir(){
//   $.getJSON("/listMort/",function(data){ console.log(data);av(data);});
//
// }



// Creation Canvas
// Le Canvas prend la taille de la fenetre -100
var windowWidth = 500;
var windowHeight =  400;
var canvas = document.createElement('canvas');
var context = canvas.getContext("2d");
canvas.id = "canvas";
// gestion taille canvas
canvas.width = window.innerWidth*4;
canvas.height = window.innerHeight*4;
document.body.appendChild(canvas);



// Classe Boule
function Boule (x, y, taille,couleur) {
  //position x ,y pour chaque boule
  this.x = x;
  this.y = y;


  //La taille d'une boule
  this.taille = taille;

  // Pour deplacer une boule Nous avons besoins de ces prochaines x et y ainsi que la vitesse
  this.vel = {
    x : _.random(-20, 20)/100,
    y : _.random(-20, 20)/100,
    min : _.random(2, 10),
    max : _.random(10, 100)/10
  }

  this.entrainer = [];
  this.color = couleurs[couleur];
}

// Affichage des boules recue par le serveur
Boule.prototype.affichage = function() {
  context.beginPath();

  //Specifier la couleur de la boule
  context.fillStyle = this.color;


  // Dessiner la boule
  context.arc(this.x,this.y, this.taille, 0, Math.PI*2);

  var i = this.entrainer.length-1;
  context.fill();

  // Affichage de la taille de la boule.
  if(this.taille!=5){
    if(this.color=="#009DDC")
      context.fillStyle = "#ffffff";
    else{
      context.fillStyle = "#ffffff";
    }
    context.font="20px Georgia";

    context.fillText(this.taille,this.x-(this.taille/4),this.y);
  }
};

// Thread pri principal d'affichage des boules
function start(){
	context.clearRect(0,0, canvas.width, canvas.height);
	_.chain(boules).each(function(p, index){
    // timer pour ne pas encombré le serveur.
    p.affichage();
	});
  //Refrecher la page
	requestAnimationFrame(start);
}
maj();
// timer pour ne pas encombré le serveur. Et créer un Thread javascript
var timer=setInterval(maj,1);

// Fonction pour avertir client
// var timer=setInterval(avertir,6);

start();
