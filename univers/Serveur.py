#!/usr/bin/env python
# coding: utf-8

from random import *
import threading
from threading import Thread
from .Boule import *
import time

class Serveur(Thread):
    def __init__(self,id):
        threading.Thread.__init__(self)
        self.__L_Boule=[]
        #Verifier l'etat du serveur
        self.__lancer= False
        self.id = id
        nombreBoule=50
        # self.__listeMorte = [] Attribut amelioration
        for x in range(nombreBoule):
            self.__L_Boule.append(MiniBoule(x))

# Gestion du lancement du Thread serveur
    def lancer(self):
        return self.__lancer
    def setLancer(self):
        self.__lancer=True


    # def envoieListeBoule(self):
    #     listM = self.__listeMorte
    #     self.__listeMorte
    #     return listM
    #
    # def ajouterMorte(self,boule):
    #     self.__listeMort.append(boules.id)


    def evoluerBoule(self):
        # Deplacement  Boule
        if(len(self.__L_Boule)!=0):
            for boule in self.__L_Boule:
                if(isinstance(boule, Boule)):
                    aux = randrange(4)
                    rapiditer=10
                    if(aux == 0 and boule.position[0]+rapiditer<1100):
                        boule.setPosition((boule.position[0]+rapiditer,boule.position[1]))
                    elif(aux==1 and boule.position[0]-rapiditer>0):
                        boule.setPosition((boule.position[0]-rapiditer,boule.position[1]))
                    elif(aux==2 and boule.position[1]+rapiditer<711):
                        boule.setPosition((boule.position[0],boule.position[1]+rapiditer))
                    elif(aux==3 and boule.position[1]-rapiditer>0):
                        boule.setPosition((boule.position[0],boule.position[1]-rapiditer))


    def run(self):
        while(True):
            self.gestionManger()
            self.evoluerBoule()


    def add(self,boule):
        self.__L_Boule.append(boule)

    def getBoules(self):
        res=[]
        for boule in self.__L_Boule:
            res.append(boule.modelisation())
        return res

    def getListeBoule(self): #Retourne la liste de Boule
        return self.__L_Boule

    def manger(self, boule, b): # Vérifie la distance entre les deux boule et apelle la fonction disparu pour la Boule qui sera mangée.
        if boule.taille >= b.taille and isinstance(boule, Boule) and boule.id!=b.id:
            dist = boule.distance(b)
            if dist < ((boule.taille*2)/3):
                if(boule.taille==500):
                    boule.setTaille((boule.taille + b.taille/2))
                return b




    def bouleEST(self, pseudo): # Supprime la Boule mangée de la liste __L_Boule
        for indice in range(len(self.__L_Boule)):
            if self.__L_Boule[indice].id == pseudo :
                return True
        return False
    def indice(self, boule): # Supprime la Boule mangée de la liste __L_Boule
        for indice in range(len(self.__L_Boule)):
            if self.__L_Boule[indice].id == boule.id :
                return indice

    def gestionManger(self):
        if(len(self.__L_Boule)!=0):
            les_mange = []
            for les_mangeur in self.__L_Boule:
                for les_manger in self.__L_Boule:
                    les_mange.append(self.manger(les_mangeur,les_manger))
                # if( isinstance(les_mangeur, Boule)and self.__L_Boule[indice(les_mangeur)].taille>=23):
                    # self.__L_Boule[indice(les_mangeur)].taille = self.__L_Boule[indice(les_mangeur)].taille - 3

            for boule_manger in les_mange:
                if(boule_manger!=None):
                    self.__L_Boule.remove(boule_manger)
                # elif(boule_manger!=None and isinstance(boule_manger, MiniBoule)):
                #     self.__L_Boule[self.indiceBoule(boule_manger)].__position=(randint(0,1100),randint(0,460))
